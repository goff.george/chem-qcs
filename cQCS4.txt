
'CHEM QCS Module4
'112020

Option Explicit

Function timeToSaveData(timeToSave As Boolean) As Boolean
On Error GoTo 0
timeToSaveData = False


'This module chacks that all of the data entry have been filled
'   in. When the condition is met, the calling worksheet triggers
'   the saveQaData code. This is done to make certain that the
'   user data is saved since they sometimes forget to click on
'   the save button.

'Are either of the results within the specified limits (Green = 43)
    If Range("m4").DisplayFormat.Interior.ColorIndex = 43 Or _
        Range("n4").DisplayFormat.Interior.ColorIndex = 43 Then
        
'If the values are in spec it is still possible for the APC values
'   to not be completely entered. To solve any partial entry issues
'   all of the data fields need to be checked. If either of the cells
'   required to calculate the APC TV are empty then we exit this routine

    If Left(ActiveSheet.Name, 3) = "APC" Then
        If Range("n15").Value = "" Or Range("o15").Value = "" Then
            Exit Function
        Else
            timeToSaveData = True
        End If
    Else
            timeToSaveData = True
    End If
   
End If


End Function

Function findTechInitials(Target As String)
'Write the initals of the technician chosen from the dropdown list

OptimizeStart

'For whatever reason, occasionally, the full name is returned from
'   this module. A loop was added to try and make sure that the
'   value returned is no more than three (3) characters

'This stops the code from grabbing the next person in the list
If Len(Target) = 3 Then
    Exit Function
End If

Dim Rng As Range
Dim itIsThreeCharacters As Boolean
Dim theInitials As String

    'If Target = Range("$e$4") Then
    
        Set Rng = Worksheets("Sheet1").Range("technicianList")
        itIsThreeCharacters = False
        theInitials = ""
        'Range("e4").Value = ""
        
            'If Application.VLookup(Target, Rng, 4, 0) = "N" Then
            '    MsgBox "This technician no longer works at AFL. Pick a current employee", vbOKCancel
            '    Range("$e$4").Value = ""
            '    Exit Function
            'End If

'            If ActiveCell.Address = "$E$4" And Range("e4") = "" Then
                theInitials = Application.VLookup(Target, Rng, 2, 0)
                Range("$e$4").Value = theInitials
'            End If
            
OptimizeEnd
    
    'End If
    
End Function

Sub firstSheetCell()

'OptimizeStart

'Set the starting point to the worksheet APC PF
'   and put the cursor to D4, Date Set. This is done to
'   make the user interface more consistent

    On Error Resume Next
    Worksheets(openingWSname).Activate
    Range("d4").Select

'OptimizeEnd

End Sub

Sub dateRangeCheck()
Dim incubationSpan, testSpan As Integer
Dim testMessage, currentTest As String
Dim dateRangeInError As Boolean


End Sub

Function IsLeapYear(currentYear As Integer) As Boolean
'Figure out if this is a leapyear

If (currentYear Mod 4) = 0 And (currentYear Mod 100) <> 0 Or ((currentYear Mod 400) = 0) Then
   IsLeapYear = True
Else
   IsLeapYear = False
End If

End Function

Sub auto_close()
'This module makes sure that any data left behind, in any of the fields
'   is erased so it will not appear the next time it is opened

OptimizeStart

Dim wbSheet As Worksheet
Dim wbSheets As Integer, i As Integer
Dim currentWorksheet As String

'Assign the needed values
wbSheets = ThisWorkbook.Sheets.Count

'Loop through all of the worksheets to update protection
For i = 1 To wbSheets

    currentWorksheet = Worksheets(i).Name
   If currentWorksheet <> "Sheet2" Then

        Sheets(currentWorksheet).Select
        wsUnlock currentWorksheet
        
        If currentWorksheet <> "Sheet1" Then
            Range("d4:h4").Value = ""
            Range("d4:h4").Interior.Color = vbWhite
        End If
        
        wsLock currentWorksheet
        
    End If
    
Next i

'Close the workbook
firstSheetCell

ThisWorkbook.Close savechanges:=True

OptimizeEnd

Application.Quit

'Stop
End Sub

Function IsFileOpen(fileFullName As String)
    Dim FileNumber As Integer
    Dim errorNum As Integer
  
    On Error Resume Next
    
    fileFullName = Application.ActiveWorkbook.Path & fileFullName
    
    FileNumber = FreeFile()   ' Assign a free file number.
    ' Attempt to open the file and lock it.
    Open fileFullName For Input Lock Read As #FileNumber
    Close FileNumber       ' Close the file.
    errorNum = Err         ' Assign the Error Number which occured
'    On Error GoTo 0        ' Turn error checking on.
    ' Now Check and see which error occurred and based
    ' on that you can decide whether file is already
    ' open
    Select Case errorNum
        ' No error occurred so ErroNum is Zero (0)
        ' File is NOT already open by another user.
        Case 0
         IsFileOpen = False
  
        Case 53
            MsgBox "The data file was not found. Please do the calculation by hand!"
  
        Case 55
            MsgBox "The data file is already open! You must close that spreadsheet before data can be saved"
            IsFileOpen = True
        
        Case 70
            MsgBox "The data file is already open! You must close that spreadsheet before data can be saved"
            IsFileOpen = True
  
        ' For any other Error occurred
        Case Else
        
        '03/15/2017
        ' This where I want to put code that will allow them to
        '   select the workbook location manually incase the
        '   the file location has changed
        'Debug.Print errorNum
            Error errorNum
    End Select
  
End Function

Sub wbUnlock()

OptimizeStart

Dim currentWorksheet As String
Dim wbSheets As Integer, i As Integer

'Assign the needed values
wbSheets = ThisWorkbook.Sheets.Count

'Loop through all of the worksheets to update protection
For i = 1 To wbSheets

'Make sure the current workbook and workSheet are available

    If Worksheets(i).Name <> "Sheet2" And Worksheets(i).Name <> "Sheet1" Then

        currentWorksheet = Worksheets(i).Name
        Sheets(currentWorksheet).Select
        wsUnlock currentWorksheet
    
    End If
    
Next i

firstSheetCell

OptimizeEnd

End Sub

Function recoveryEntry() As Boolean

If recoveryEntryFailure Then

    'Final msgBox to let them know why they have been thrown out
    MsgBox "Zero (0) values not allowed", vbOKOnly
    
    'Close the workbook
    Application.DisplayAlerts = False
    ActiveWorkbook.Close savechanges:=True
    Application.DisplayAlerts = True
    recoveryEntryFailure = False
    
Else

    If Range("g4").Value <= 0 Or Range("h4").Value <= 0 Then
        
        'Assumes an incorrect value was entered - ZERO (0)
        recoveryEntryFailure = True
        MsgBox "Zero (0) values are not not possible. Try again...", vbOKOnly
        
    End If
    Range("g4").Select
    Range("g4").Value = ""
End If

End Function

Sub populateEmployeeListBox(sheetName, nameArray)

OptimizeStart
    
        Sheets(sheetName).Select
        Debug.Print sheetName
        
        If sheetName <> "Sheet1" And sheetName <> "Sheet2" Then
        
        wsUnlock ActiveSheet.Name
        
        With Worksheets(sheetName)
            With Range("e4").Validation
                .Delete
                .Add Type:=xlValidateList, AlertStyle:=xlValidAlertStop, _
                     Operator:=xlBetween, Formula1:=Join(nameArray, ",")
            End With
        End With
        
        Range("d4").Select
        
        wsLock ActiveSheet.Name
        
        End If
        
OptimizeEnd

End Sub

Sub prepWorkbook()

OptimizeStart

Dim ws As Worksheet

'If something goes wrong, display the error number and description
On Error GoTo errTrap

Dim i As Long
'Loop through all of the sheets in the workbook
For i = 1 To ActiveWorkbook.Worksheets.Count

Dim wsName As String
wsName = Worksheets(i).Name

If wsName <> "Sheet2" And wsName <> "Sheet1" Then
'This unlocks the workbook but still protects the element
    wsUnlock (wsName)
End If

If i = ActiveWorkbook.Worksheets.Count Then
    OptimizeEnd
    Exit Sub
End If

Next i

OptimizeEnd

errTrap:
If Err.Number > 0 Then
    
    MsgBox "Error number - " & Err.Number & vbCrLf & Err.Description
End If

End Sub

Sub unprepWorkbook()

OptimizeStart

'If something goes wrong, display the error number and description
On Error GoTo errTrap

Dim ws As Worksheet
'Loop through all of the sheets in the workbook

Dim i As Long

For i = 1 To ActiveWorkbook.Worksheets.Count

Dim wsName As String
wsName = Worksheets(i).Name
Debug.Print wsName

If wsName <> "Sheet2" Then
'This locks the workbook
    
    Call wsLock(wsName)
End If

If i = ActiveWorkbook.Worksheets.Count Then
    OptimizeEnd
    Exit Sub
End If

Next i

OptimizeEnd

errTrap:
If Err.Number > 0 Then
    
    MsgBox "Error number - " & Err.Number & vbCrLf & Err.Description
End If

End Sub

Sub initPublicVars()

OptimizeStart

Dim spaceInName As Long

thisWBname = ThisWorkbook.Name 'Public variable in 'Module1'
spaceInName = InStr(thisWBname, " ")
tempName = Left(thisWBname, spaceInName - 6) 'Public variable in 'Module1'
dataWBname = "\" & tempName & "data.xlsm" 'Public variable in 'Module1'
thisWBsheets = ActiveWorkbook.Worksheets.Count
wsNames 'Build Array of WS names to use in the code
openingWSname = nameList(0) 'Public variable in 'Module1'

OptimizeEnd

End Sub

Sub wsNames()

OptimizeStart

Dim i As Long
i = 0

'Get a list of all the WS's names
Dim ws As Worksheet

'Loop through all of the sheets in the workbook
For Each ws In ActiveWorkbook.Worksheets
    
    ReDim Preserve nameList(i)
    nameList(i) = ws.Name
    i = i + 1
    
Next ws

OptimizeEnd

Exit Sub

End Sub

Sub clearPublicVars()

Dim spaceInName As Long
Dim tempName As String

Application.StatusBar = "Initializing PUBLIC variables..."

thisWBname = ""
spaceInName = 0
tempName = ""
dataWBname = ""
thisWBsheets = 0
wsNames 'Build Array of WS names to use in the code
openingWSname = ""

Application.StatusBar = ""

End Sub

Sub hideLock(wsName)
ActiveWorkbook.Worksheets("Sheet2").Visible = xlSheetVisible
ActiveWorkbook.Worksheets(wsName).Protect Password:=ThisWorkbook.Worksheets("Sheet2").Range("a1").Value, UserInterFaceOnly:=True
ActiveWorkbook.Worksheets(wsName).Protect AllowUsingPivotTables:=True
ActiveWorkbook.Worksheets("Sheet2").Visible = xlSheetVeryHidden
End Sub

Sub wsLock(wsName As String)
If wsName = "Sheet2" Then hideLock wsName
    ActiveWorkbook.Worksheets(wsName).Cells.Locked = True
    ActiveWorkbook.Worksheets(wsName).Cells.Locked = False
    ActiveWorkbook.Worksheets(wsName).Protect Password:=ThisWorkbook.Worksheets("Sheet2").Range("a1").Value, UserInterFaceOnly:=True
    ActiveWorkbook.Worksheets(wsName).Cells.Locked = True
    
    If wsName <> "Sheet1" And wsName <> "Sheet2" Then
        ActiveWorkbook.Worksheets(wsName).Range("d4:h4").Locked = False
    End If
End Sub

Sub hideUnLock(wsName)
ActiveWorkbook.Worksheets("Sheet2").Visible = xlSheetVisible
ActiveWorkbook.Worksheets(wsName).Unprotect Password:=ThisWorkbook.Worksheets("Sheet2").Range("a1").Value
ActiveWorkbook.Worksheets("Sheet2").Visible = xlSheetVeryHidden
End Sub

Sub wsUnlock(wsName As String)
If wsName = "Sheet2" Then hideUnLock (wsName)
ActiveWorkbook.Worksheets(wsName).Unprotect Password:=ThisWorkbook.Worksheets("Sheet2").Range("a1").Value2
End Sub

'   ANY MODULE Code

Sub SetTimer()
    downTime = Now + TimeValue("00:05:00")  'Set how long the WB can remain open
    Application.OnTime EarliestTime:=downTime, _
      Procedure:="ShutDown", Schedule:=True
End Sub

Sub StopTimer()
    On Error Resume Next
    Application.OnTime EarliestTime:=downTime, _
      Procedure:="ShutDown", Schedule:=False
 End Sub

Sub ShutDown()
    Application.DisplayAlerts = False
    ThisWorkbook.Close
    Application.Quit
End Sub
